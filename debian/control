Source: owfs
Maintainer: Vincent Danjean <vdanjean@debian.org>
Section: electronics
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13),
               libfuse-dev [!hurd-any],
               swig,
               tcl-dev,
               libusb-1.0-0-dev,
               ed,
               php-cli,
               php-dev,
               systemd-dev,
               groff-base,
               pkgconf,
               libavahi-client-dev,
               libftdi1-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian/owfs
Vcs-Git: https://salsa.debian.org/debian/owfs.git
Homepage: http://owfs.org/
Rules-Requires-Root: no

Package: owfs-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         adduser,
         ucf
Breaks: libow-2.8-15 (<<2.9)
Description: common files used by any of the OWFS programs
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 This package contains the common files that are used by any of the
 OWFS programs.

Package: owfs
Architecture: all
Depends: ${misc:Depends},
         owfs-fuse (>= ${binary:Version}),
         owserver (>= ${binary:Version}),
         owhttpd (>= ${binary:Version}),
         owftpd (>= ${binary:Version})
Suggests: owfs-doc
Description: Dallas 1-wire support
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 This package is a metapackage depending on various other OWFS packages.
 Installing this package gets you a FUSE daemon, server arbitrates access
 to the bus from multiple client processes, small ftp and webserver.

Package: libow-3.2-4t64
Provides: ${t64:Provides}
Replaces: libow-3.2-4
Breaks: libow-3.2-4 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends},
         owfs-common (>= ${source:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: 1-Wire File System full library
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 This package includes the ow library that is able to talk directly to 1-wire
 buses.

Package: libowcapi-3.2-4t64
Provides: ${t64:Provides}
Replaces: libowcapi-3.2-4
Breaks: libowcapi-3.2-4 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: 1-Wire File System C library
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 This package includes the C API that allows one to talk directly to 1-wire
 buses (through the libow library).

Package: libow-dev
Architecture: any
Section: libdevel
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libow-3.2-4t64 (= ${binary:Version}),
         libowcapi-3.2-4t64 (= ${binary:Version})
Multi-Arch: same
Description: 1-Wire File System (development files)
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 This package includes development libraries and C header files.

Package: libownet-3.2-4t64
Provides: ${t64:Provides}
Replaces: libownet-3.2-4
Breaks: libownet-3.2-4 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: owserver protocol library
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 This package includes the ownet library that is able to talk to a owserver
 in order to get/send 1-wire information.

Package: libownet-dev
Architecture: any
Section: libdevel
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libownet-3.2-4t64 (= ${binary:Version})
Multi-Arch: same
Description: owserver protocol library (development files)
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 This package contains the development libraries and interfaces
 to access a remote owserver.

Package: owserver
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends},
         owfs-common (>= 3.2p3-1~),
Suggests: avahi-daemon
Description: Backend server for 1-Wire control
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 OWserver arbitrates access to the 1-Wire bus from multiple client processes.
 The physical bus is usually connected to a serial or USB port,
 and other processes connect to owserver over network sockets (tcp port).
 Communication can be local or over a network.

Package: ow-shell
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: owserver
Description: shell utilities to talk to an 1-Wire owserver
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 This package has several command line programs to talk to an owserver.
 Included programs are :
  * owdir: list 1-wire devices or properties
  * owread: read 1-wire value
  * owget: combines owdir and owread
  * owwrite: set a 1-wire value
  * owexist: check if a 1-wire server is reachable
  * owpresent: check if a 1-wire device is connected

Package: ow-tools
Architecture: all
Depends: ${misc:Depends},
         tk
Suggests: owserver
Breaks: ow-shell (<< 2.9p5)
Replaces: ow-shell (<< 2.9p5)
Multi-Arch: foreign
Description: tools to monitor or inspect a ow-server link
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 This package includes two TclTk tools:
  * owtap: inspect network transmission of the owserver protocol
  * owmon: show statistics and setting for an owserver

Package: owfs-fuse
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         owfs-common (>= 3.2p3-1~)
Recommends: owserver,
            fuse
Suggests: owhttpd,
          owftpd
Description: 1-Wire filesystem
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 OWFS is a userspace virtual filesystem exposing all 1-Wire properties
 mapped into a filesystem.

Package: owhttpd
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends},
         owfs-common (>= 3.2p3-1~),
Recommends: owserver
Description: HTTP daemon providing access to 1-Wire networks
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 HTTP daemon providing access to 1-Wire networks.

Package: owftpd
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends},
         owfs-common (>= 3.2p3-1~),
Recommends: owserver
Description: FTP daemon providing access to 1-Wire networks
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 FTP daemon providing access to 1-Wire networks.

Package: libow-php
Provides: ${t64:Provides}
Replaces: libow-php7, libow-php7t64
Breaks: libow-php7 (<< ${source:Version}), libow-php7t64 (<< ${source:Version})
Architecture: any
Section: php
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${php:Depends}
Description: Dallas 1-wire support: PHP bindings
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 PHP bindings for the OWFS 1-wire support library have been produced
 with SWIG and allow access to libow* functions from PHP code.

Package: libownet-php
Architecture: all
Section: web
Depends: php | php-cli,
         ${misc:Depends}
Description: Dallas 1-wire support: PHP OWNet library
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 The PHP OWNet library lets you access owserver and allows reading,
 writing and listing the 1-wire bus.

Package: libow-perl
Architecture: any
Section: perl
Depends: ${shlibs:Depends},
         ${perl:Depends},
         ${misc:Depends}
Multi-Arch: same
Description: Dallas 1-wire support: Perl5 bindings
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 Perl bindings for the OWFS 1-wire support library have been produced
 with SWIG and allow access to libow functions from perl code.

Package: libownet-perl
Architecture: all
Section: perl
Depends: ${shlibs:Depends},
         ${perl:Depends},
         ${misc:Depends}
Multi-Arch: foreign
Description: Perl module for accessing 1-wire networks
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 OWNet.pm is a perl module for accessing 1-wire sensors through an owserver.

Package: libow-tcl
Architecture: any
Section: interpreters
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Dallas 1-wire support: Tcl bindings
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 This package contains Tcl bindings to the OWFS core library (libow),
 which is used to access 1-Wire networks

Package: owfs-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: owfs
Multi-Arch: foreign
Description: Dallas 1-wire support: Documentation for owfs
 The 1-Wire bus is a cheap low-speed bus for devices like weather
 sensors, access control, etc. It can be attached to your system via
 serial, USB, I2C, and other interfaces.
 .
 Documentation for OWFS in 'man' format.
 This package contains the manpages for 1-Wire, including the
 device-specific manpages.
